/* jshint esversion: 6 */
class HamburgerException extends Error {
    constructor(message) {
        super(message);
    }
}

class Hamburger {
    constructor(size, stuffing) {
        try {
            if (!size || !stuffing) {
                throw new HamburgerException("no size or stuffing given");
            } else if (!(size instanceof Object) || !(stuffing instanceof Object)) {
                throw new HamburgerException("ivalid type of size or stuffing given");
            } else if (size.name != Hamburger.SIZE_SMALL.name && size.name != Hamburger.SIZE_LARGE.name) {
                throw new HamburgerException("select correct size: Small or Large");
            } else if (stuffing.name != Hamburger.STUFFING_CHEESE.name && stuffing.name != Hamburger.STUFFING_SALAD.name && size.name != Hamburger.STUFFING_POTATO.name) {
                throw new HamburgerException("select correct stuffing: Cheese, Salad or Potato");
            } else {
                this.size = size;
                this.stuffing = stuffing;
                this.toppings = [];
            }
        } catch (e) {
            console.log(e.message);
        }
    }
    static get SIZE_SMALL() {
        const SIZE_SMALL = {
            name: 'SIZE_SMALL',
            price: 50,
            calories: 20
        }
        return Object.freeze(SIZE_SMALL);
    }
    static get SIZE_LARGE() {
        const SIZE_LARGE = {
            name: 'SIZE_SMALL',
            price: 100,
            calories: 40
        }
        return Object.freeze(SIZE_LARGE);
    }


    static get STUFFING_CHEESE() {
        const STUFFING_CHEESE = {
            name: 'STUFFING_CHEESE',
            price: 10,
            calories: 20
        }
        return Object.freeze(STUFFING_CHEESE);
    }
    static get STUFFING_SALAD() {
        const STUFFING_SALAD = {
            name: 'STUFFING_SALAD',
            price: 20,
            calories: 5
        }
        return Object.freeze(STUFFING_SALAD);
    }
    static get STUFFING_POTATO() {
        const STUFFING_POTATO = {
            name: 'STUFFING_POTATO',
            price: 15,
            calories: 10
        }
        return Object.freeze(STUFFING_POTATO);
    }
    static get TOPPING_MAYO() {
        const TOPPING_MAYO = {
            name: 'TOPPING_MAYO',
            price: 20,
            calories: 5
        }
        return Object.freeze(TOPPING_MAYO);
    }
    static get TOPPING_SPICE() {
        const TOPPING_SPICE = {
            name: 'TOPPING_SPICE',
            price: 15,
            calories: 0
        }
        return Object.freeze(TOPPING_SPICE);
    }
    // functions for Hamburger

    addTopping(topping) {
        try {
            if (!topping) {
                throw new HamburgerException("no topping given");
            } else if (topping.name != Hamburger.TOPPING_MAYO.name && topping.name != Hamburger.TOPPING_SPICE.name) {
                throw new HamburgerException("select correct topping: TOPPING_MAYO or TOPPING_SPICE");
            } else if (this.toppings.some((item) => item.name == topping.name)) {
                throw new HamburgerException("You've already added this topping!");
            } else {
                this.toppings.push(topping);
            }

        } catch (e) {
            console.log(e.message);
        }
    }
    removeTopping(topping) {
        try {
            if (!topping) {
                throw new HamburgerException("no topping specified");
            } else if (this.toppings.every((item) => item.name !== topping.name)) {
                throw new HamburgerException("this topping wasn't added");
            } else {
                this.toppings.splice(this.toppings.indexOf(topping), 1);

            }
        } catch (e) {
            console.log(e.message);
        }
    }

    getSize() {
        return this.size;
    }
    getToppings() {
        return this.toppings;
    }
    calculatePrice() {
        let bunPrice = this.size.price;
        let stuffingPrice = this.stuffing.price;
        let toppingsPrice = 0;
        for (let i = 0; i < this.toppings.length; i++) {
            toppingsPrice += this.toppings[i].price;
        }
        let totalPrice = bunPrice + stuffingPrice + toppingsPrice;
        return totalPrice;
    }

    calculateCalories() {
        let bunCalories = this.size.calories;
        let stuffingCalories = this.stuffing.calories;
        let toppingsCalories = 0;
        for (let i = 0; i < this.toppings.length; i++) {
            toppingsCalories += this.toppings[i].calories;
        }
        let totalCalories = bunCalories + stuffingCalories + toppingsCalories;
        return totalCalories;
    };
}
let h1 = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_CHEESE);
h1.addTopping(Hamburger.TOPPING_MAYO);
h1.addTopping(Hamburger.TOPPING_MAYO);
h1.removeTopping(Hamburger.TOPPING_MAYO);
h1.removeTopping(Hamburger.TOPPING_MAYO);
h1.calculateCalories();
h1.calculatePrice();

let h2 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_SALAD);


// mistake checker 
let h3 = new Hamburger ('kisyash','pishyash'); // invalid type of z or st. ;
h2.removeTopping(); // no topping specified
h2.addTopping('sdgds'); // select correct topping: TOPPING_MAYO or TOPPING_SPICE
