$(document).ready(function(){
    $('.burger-off').hide();
    // $('.dropdown-menu').hide();

    // dropdown | burger menu creation

    $('.burger-space').click(function(evt){
        evt.preventDefault();
        if($('.burger-off').is(':hidden')){
            $('.burger-on').hide();
            $('.burger-off').show();
            $('.dropdown-menu').slideDown(500);
        } else {
            $('.burger-on').show();
            $('.burger-off').hide();
            $('.dropdown-menu').slideUp(500);
        }
    })
    $(document).click(function (evt) {
        if($(evt.target).closest('.burger-space').length) return;
        $('.burger-on').show();
        $('.burger-off').hide();
        $('.dropdown-menu').slideUp(500);
        evt.preventDefault();
    })

    // if(window.matchMedia('(min-width: 980px)').matches){
    //     $('.menu-option').hide();
    //     $('.menu-option-desk').show()
    // } else{
    //     $('.menu-option').show();
    //     $('.menu-option-desk').hide();
    // }


});
