


// constructor
function Hamburger(size, stuffing) {
    this.size = size;
    this.stuffing = stuffing;
    this.toppings = [];
    
}
// custom mistake
function HamburgerException(message) {
    this.message = message;
}
HamburgerException.prototype.__proto__ = Error.prototype;



// sizes
Hamburger.SIZE_SMALL = {
    param: "SIZE_SMALL",
    price: 50,
    calories: 20
};
Hamburger.SIZE_LARGE = {
    param: "SIZE_LARGE",
    price: 100,
    calories: 40
};

// stuffings
Hamburger.STUFFING_CHEESE = {
    param: "STUFFING_CHEESE",
    price: 10,
    calories: 20
};
Hamburger.STUFFING_SALAD = {
    param: "STUFFING_SALAD",
    price: 20,
    calories: 5
};
Hamburger.STUFFING_POTATO = {
    param: 'STUFFING_POTATO',
    price: 15,
    calories: 10
};


// toppings
Hamburger.TOPPING_MAYO = {
    param: 'TOPPING_MAYO',
    price: 20,
    calories: 5
};
Hamburger.TOPPING_SPICE = {
    param: 'TOPPING_SPICE',
    price: 15,
    calories: 0
};
// freezing Hamburger's properties



// adding toppings
Hamburger.prototype.addTopping = function (topping) {
    try {
        if (this.toppings.includes(topping)) {
            throw new HamburgerException('This topping is already added');
        } else {
            this.toppings.push(topping);
        }
    } catch (e) {
        console.log(e.message);
    }
};

// removing toppings
Hamburger.prototype.removeTopping = function (topping) {
    try {
            if (this.toppings.includes(topping)){
                this.toppings.splice(this.toppings.indexOf(topping), 1);
            } else {
                throw new HamburgerException('Nothing to remove');
            }
    } catch (e) {
            console.log(e.message);
        }
    };

// what toppings are added
Hamburger.prototype.getToppings = function () {
    return this.toppings;
};

// get size of your choice 
Hamburger.prototype.getSize = function () {
    return this.size.param;
};
// get stuffing of your choice
Hamburger.prototype.getStuffing = function () {
    return this.stuffing.param;
};

//How much it cost?
Hamburger.prototype.calculatePrice = function () {
    var bunPrice = this.size.price;
    var stuffingPrice = this.stuffing.price;
    var toppingsPrice = 0;
    for (var i = 0; i < this.toppings.length; i++) {
        toppingsPrice += this.toppings[i].price;
    }
    var totalPrice = bunPrice + stuffingPrice + toppingsPrice;
    return totalPrice;
};

//Calories?
Hamburger.prototype.calculateCalories = function () {
    var bunCalories = this.size.calories;
    var stuffingCalories = this.stuffing.calories;
    var toppingsCalories = 0;
    for (var i = 0; i < this.toppings.length; i++) {
        toppingsCalories += this.toppings[i].calories;
    }
    var totalCalories = bunCalories + stuffingCalories + toppingsCalories;
    return totalCalories;
};

// mistake checker

freeze();
var hamburger1 = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_CHEESE);
console.log(hamburger1);

function freeze(){
    var propNames = Object.getOwnPropertyNames(Hamburger);
    propNames.forEach(function(name){
        var property = Hamburger[name];
        if (property instanceof Object) {
            Object.freeze(property);
        }
    });
}

hamburger1.addTopping(Hamburger.TOPPING_MAYO);
hamburger1.addTopping(Hamburger.TOPPING_MAYO);
hamburger1.addTopping(Hamburger.TOPPING_MAYO);

// hamburger1.addTopping(Hamburger.TOPPING_MAYO);
// hamburger1.addTopping(Hamburger.TOPPING_SPICE);
// console.log(hamburger1.getToppings());
// console.log(hamburger1.calculatePrice());
// let hamburger2 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_POTATO);
// console.log(hamburger2);
// hamburger2.addTopping(Hamburger.TOPPING_SPICE);
// hamburger2.addTopping(Hamburger.TOPPING_MAYO);
// console.log(hamburger2.calculateCalories());
// console.log(hamburger2.getSize());
// console.log(hamburger2.getStuffing());